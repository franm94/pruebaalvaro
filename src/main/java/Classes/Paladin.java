package Classes;

/*
Atributos:
Nombre
Vida
Fuerza
Defensa

Funciones:
Andar
Atacar
Tomar Pocion
Sentarse
Saludar
*/

public class Paladin {
    // Atributos PRIVADOS
    private String nombre;
    private Integer vida;
    private Integer fuerza;
    private Integer defensa;

    public Paladin(String nombre){
        this.nombre = nombre;
        this.vida = 100;
        this.fuerza = 50;
        this.defensa = 70;
    }

    public Paladin(String nombre, Integer vida){
        this.nombre = nombre;
        this.vida = vida;
        this.fuerza = 50;
        this.defensa = 70;
    }

    // Getters y Setters
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Integer getVida() {
        return vida;
    }

    public void setVida(Integer vida) {
        this.vida = vida;
    }

    public Integer getFuerza() {
        return fuerza;
    }

    public void setFuerza(Integer fuerza) {
        this.fuerza = fuerza;
    }

    public Integer getDefensa() {
        return defensa;
    }

    public void setDefensa(Integer defensa) {
        this.defensa = defensa;
    }

    public void saludar(){
        System.out.println("Heey, que tal?!");
    }

    public void setCurarse(Integer pocion){
        this.vida = this.vida + pocion;
    }

}
